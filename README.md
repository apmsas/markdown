- [Kubernetes + Ansible](#kubernetes--ansible)
  - [Ansible](#ansible)
  - [Configuracion de hosts](#configuracion-de-hosts)
    - [Root passwords](#root-passwords)
    - [Usuario admin / Firewall](#usuario-admin--firewall)
  - [Kubespray](#kubespray)
    - [SSH en los nodos](#ssh-en-los-nodos)
    - [Ejecucion de playbook](#ejecucion-de-playbook)
    - [Permitir kubectl sin sudo](#permitir-kubectl-sin-sudo)
  - [Storage](#storage)
    - [Rook](#rook)
      - [Instalacion](#instalacion)
      - [Creacion de cluster](#creacion-de-cluster)
      - [Toolbox](#toolbox)
      - [Shared filesystem](#shared-filesystem)
      - [StorageClass](#storageclass)
      - [Dashboard](#dashboard)
        - [Credentials](#credentials)
  - [Servicios](#servicios)
    - [Cert-Manager + Let's Encrypt](#cert-manager--lets-encrypt)
    - [Nginx-Ingress](#nginx-ingress)
      - [Instalacion por chart](#instalacion-por-chart)
    - [LDAP](#ldap)
      - [OpenLDAP](#openldap)
        - [Archivo values](#archivo-values)
        - [Instalacion por chart](#instalacion-por-chart-1)
      - [PHPLdap Admin](#phpldap-admin)
        - [Instalacion por chart](#instalacion-por-chart-2)
      - [LDAP user manager](#ldap-user-manager)
        - [yaml de deployment](#yaml-de-deployment)
        - [Instalacion](#instalacion-1)
        - [Configuracion](#configuracion)
    - [Prometheus](#prometheus)
      - [Archivo values](#archivo-values-1)
      - [Instalacion por chart](#instalacion-por-chart-3)
    - [Nexus](#nexus)
      - [Temp ssl secret](#temp-ssl-secret)
      - [Archivo values](#archivo-values-2)
      - [Instalacion por chart](#instalacion-por-chart-4)
        - [Chart](#chart)
      - [Post-instalacion](#post-instalacion)
      - [Migracion por api](#migracion-por-api)
      - [Migracion por storage](#migracion-por-storage)
      - [Troubleshooting](#troubleshooting)
    - [Harbor](#harbor)
      - [Archivo values](#archivo-values-3)
      - [Instalacion por chart](#instalacion-por-chart-5)
    - [Gitlab](#gitlab)
      - [Archivo values](#archivo-values-4)
      - [Instalacion por chart](#instalacion-por-chart-6)
      - [Gitlab webserver](#gitlab-webserver)
      - [Minio](#minio)



# Kubernetes + Ansible
## Ansible
docker 

## Configuracion de hosts
### Root passwords
1. se crea el archivo *./ansible/kubespray/inventory-cloud/vault/unencrypted.yml* con las contraseñas del root de cada host; modelo: 

```yaml
root_passwords:
  master1: ******
  master2: ******
  master3: ******
  worker1: ******
  worker2: ******
```

2. Se crea el vault con el archivo con contraseñas; comando: 

       ansible-vault encrypt /home/ansible/git/ansible/kubespray/inventory-cloud/vars/unencrypted.yml --output /home/ansible/git/ansible/kubespray/inventory-cloud/vars/root_passwords

ansible-vault solicito la contraseña del baul, la cual fue completada; esa contraseña no puede quedar expuesta

3. se modifico el playbook **initial.yaml** para que lea las variables del vault creado y para que use la contraseña de ssh almacenada ahi 

```yaml
vars_files:
  - "{{ inventory_dir }}/vars/root_passwords"
vars:
  ansible_password: "{{ root_passwords[inventory_hostname] }}"
```

### Usuario admin / Firewall
1. se configura el archivo *ansible/kubespray/inventory-cloud/hosts-ini.yaml* para poder acceder a los nodos
2. se ejecuta el playbook *./initial.yaml", el cual genera un usuario admin y un private key para cada nodo, configura el firewall y finalmente obtiene las public keys para poder entrar sin utilizar contraseña

```bash
export ANSIBLE_CONFIG=/home/ansible/git/ansible/kubespray/ansible.cfg
ansible-playbook --ask-vault-pass /home/ansible/git/ansible/kubespray/initial.yaml -i /home/ansible/git/ansible/kubespray/inventory-cloud/hosts-ini.yaml --become 
```

## Kubespray
### SSH en los nodos

```sh
ssh admin@master01.gyfcloud.com.ar -p 2022 -i /home/ansible/git/ansible/kubespray/inventory-cloud/ssh/master1_rsa
ssh admin@master02.gyfcloud.com.ar -p 2023 -i /home/ansible/git/ansible/kubespray/inventory-cloud/ssh/master2_rsa
ssh admin@master03.gyfcloud.com.ar -p 2024 -i /home/ansible/git/ansible/kubespray/inventory-cloud/ssh/master3_rsa
ssh admin@node1.gyfcloud.com.ar -p 2025 -i /home/ansible/git/ansible/kubespray/inventory-cloud/ssh/worker1_rsa
ssh admin@node2.gyfcloud.com.ar -p 2026 -i /home/ansible/git/ansible/kubespray/inventory-cloud/ssh/worker2_rsa
```

### Ejecucion de playbook

1. se configura el archivo *ansible/kubespray/inventory-cloud/hosts.yaml* seteando el **ansible_host** y los grupos 
2. se configura el archivo de variables para los hosts *ansible/kubespray/inventory-cloud/group_vars/k8s-cluster* para definir el comportamiento de kubespray
3. se ejecuta el playbook *./cluster.yml*, el cual instala kubernetes segun las directivas pasadas en el archivo de variables:

```bash
export ANSIBLE_CONFIG=/home/ansible/git/ansible/kubespray/ansible.cfg
ansible-playbook /home/ansible/git/ansible/kubespray/cluster.yml -i /home/ansible/git/ansible/kubespray/inventory-cloud/hosts.yaml --become 
```

### Permitir kubectl sin sudo
1. se ejecuta el playbook *./nonsudo.yml*, el cual copia el archivo **/etc/kubernetes/admin.conf** a la carpeta raiz del usuario con el que se ingresa, para permitir la ejecucion de kubectl sin tener que poner sudo antes

```bash
export ANSIBLE_CONFIG=/home/ansible/git/ansible/kubespray/ansible.cfg
ansible-playbook /home/ansible/git/ansible/kubespray/nonsudo.yml -i /home/ansible/git/ansible/kubespray/inventory-cloud/hosts.yaml --become 
```

## Storage

### Rook
se usa rook-ceph como storage provider. en el repo se pueden ver los distintos documentos necesarios para entender el funcionamiento:

https://github.com/rook/rook/tree/release-1.4/Documentation


#### Instalacion
se siguen las isntrucciones de https://github.com/rook/rook/blob/release-1.4/Documentation/helm-operator.md

1. se agrega el repo de helm 

```bash      
helm repo add rook-release https://charts.rook.io/release
```

2. se instala el chart

```bash
kubectl create ns rook-ceph
helm install --namespace rook-ceph rook-ceph rook-release/rook-ceph
```

#### Creacion de cluster
se siguen las instrucciones de https://github.com/rook/rook/blob/release-1.4/Documentation/ceph-quickstart.md

1. se aplica el contenido del archivo *./ansible/kubespray/inventory-cloud/rook-cluster.yml* con:
        
```bash
kubectl apply -f ./ansible/kubespray/inventory-cloud/rook-cluster.yml # (contemplar la ruta del archivo segun donde se esta parado)
```

#### Toolbox
se siguen las instrucciones de https://github.com/rook/rook/blob/release-1.4/Documentation/ceph-toolbox.md

1. se aplica el contenido del archivo *./ansible/kubespray/inventory-cloud/rook-toolbox.yml* con:

```bash
kubectl apply -f ./ansible/kubespray/inventory-cloud/rook-toolbox.yml # (contemplar la ruta del archivo segun donde se esta parado)
```

2. se ingresa al toolbox (una vez que este corriendo); para ello:
  
    1. ```kubectl get pods -n rook-ceph ```
    2. tomar el nombre del pod que empieza con **rook-ceph-tools**; en este caso: **rook-ceph-tools-6b4889fdfd-9nfz8**
    3. ```kubectl exec -it -n rook-ceph rook-ceph-tools-6b4889fdfd-9nfz8 -- bash ```

3. dentro del toolbox, se ejecuta el siguiente comando para revisar el estado del cluster:   

```bash
ceph status
```

4. el resultado es:
    ```
    cluster:
      id:     a91e810b-0b31-4eb1-ae01-3c9faf16043b
      health: HEALTH_WARN
              clock skew detected on mon.b
              Degraded data redundancy: 1/3 objects degraded (33.333%), 1 pg degraded, 1 pg undersized
              OSD count 2 < osd_pool_default_size 3

    services:
      mon: 3 daemons, quorum a,b,c (age 12m)
      mgr: a(active, since 11m)
      osd: 2 osds: 2 up (since 12m), 2 in (since 12m)

    data:
      pools:   1 pools, 1 pgs
      objects: 1 objects, 0 B
      usage:   2.0 GiB used, 198 GiB / 200 GiB avail
      pgs:     1/3 objects degraded (33.333%)
              1 active+undersized+degraded
    ```

5. en este caso, se muestra que la salud tiene un warning, el cual indica uqe la cantidad de OSD == 2 es menor a osd_pool_default_size == 3; esto se da porque actualmente tenemos 2 workers en el cluster, y los masters tienen etiquetas que previene que kubernetes genere pods en ellos, que no sean del sistema

#### Shared filesystem
se siguen las instrucciones de https://github.com/rook/rook/blob/release-1.4/Documentation/ceph-filesystem.md

**tener en cuenta el nombre del filesystem que se elija (en este caso rook-filesystem) ya que se usara en el storageClass:
```yaml
metadata:
  name: rook-filesystem
  namespace: rook-ceph
```

1. se aplica el contenido del archivo *./ansible/kubespray/inventory-cloud/rook-filesystem.yml* con:

```bash
kubectl apply -f ./ansible/kubespray/inventory-cloud/rook-filesystem.yml # (contemplar la ruta del archivo segun donde se esta parado)
```

#### StorageClass
se siguen las instrucciones de https://github.com/rook/rook/blob/release-1.4/Documentation/ceph-filesystem.md
**usar el nombre del filesystem elegido en el paso anterior para las siguientes propiedades del storageClass:**

```yaml
parameters:
  # CephFS filesystem name into which the volume shall be created
  fsName: rook-filesystem
  # Ceph pool into which the volume shall be created
  # Required for provisionVolume: "true"
  pool: rook-filesystem-data0
```

> el pool va a ser el nombre del filesystem más "-data0"

contemplar que se esta definiendo la annotation **storageclass.kubernetes.io/is-default-class** en true, por lo que el StorageClass creado va a ser el que se use por defecto al crear persistentVolumeClaims    
```yaml
metadata:
  name: rook-cephfs
  annotations:
    storageclass.kubernetes.io/is-default-class: "true"
```
#### Dashboard
se siguen los pasos de https://github.com/rook/rook/blob/release-1.4/Documentation/ceph-dashboard.md
se crea el ingress para exponer una url publica:
```yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: rook-ceph-mgr-dashboard
  namespace: rook-ceph
  annotations:
    kubernetes.io/ingress.class: "nginx"
spec:
  rules:
  - host: rook-dashboard.cluster.gyfcloud.com.ar
    http:
      paths:
      - path: /
        backend:
          serviceName: rook-ceph-mgr-dashboard
          servicePort: 7000 # este es el puerto HTTP; una vez que se tenga habilitado HTTPS (con lets encrypt) se debera usar el puerto 8443 y en la creacion del cluster contemplar la propiedad 'ssl: false' y cambiarla a TRUE
```
##### Credentials

```bash
kubectl -n rook-ceph get secret rook-ceph-dashboard-password -o jsonpath="{['data']['password']}" | base64 --decode
```

## Servicios
### Cert-Manager + Let's Encrypt
se siguen los pasos de https://hub.helm.sh/charts/jetstack/cert-manager
```bash
kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.0.1/cert-manager.crds.yaml
kubectl create ns cert-manager
helm repo add jetstack https://charts.jetstack.io
helm install cert-manager --namespace cert-manager jetstack/cert-manager
```

se crea el ClusterIssuer para emitir los certificados
```yaml
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: letsencrypt-issuer
spec:
  acme:
    # The ACME server URL
    server: https://acme-staging-v02.api.letsencrypt.org/directory
    # Email address used for ACME registration
    email: devops@gyf.com.ar
    # Name of a secret used to store the ACME account private key
    privateKeySecretRef:
      name: letsencrypt-staging
    # Enable the HTTP-01 challenge provider
    solvers:
    - http01:
        ingress:
          class: nginx
```

### Nginx-Ingress
#### Instalacion por chart

```bash
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm upgrade --install ingress-nginx ingress-nginx/ingress-nginx
```
### LDAP
#### OpenLDAP
##### Archivo values
```yaml
persistence:
  enabled: true 
env:
  LDAP_ORGANISATION: Gyf 
  LDAP_DOMAIN: gyfcloud.com.ar 
  LDAP_TLS: "false"
  LDAP_TLS_VERIFY_CLIENT: never 
  LDAP_BASE_DN: dc=gyfcloud,dc=com,dc=ar
test:
  enabled: true 
```
##### Instalacion por chart
```bash
helm repo add stable https://kubernetes-charts.storage.googleapis.com
helm upgrade --install openldap stable/openldap -f openldap-values.yml
```
ldapsearch -x -D "cn=admin,dc=gyfcloud,dc=com,dc=ar" -W
ldapsearch -b "dc=gyfcloud,dc=com,dc=ar" -D "cn=admin,dc=gyfcloud,dc=com,dc=ar" -w 3xXqvNcI2TTdQroGOq2CcMVORMF1Nx85

admin password:
```bash
kubectl get secret openldap -o jsonpath="{.data.LDAP_ADMIN_PASSWORD}" | base64 --decode
```

#### PHPLdap Admin
##### Instalacion por chart
```bash
helm repo add cetic https://cetic.github.io/helm-charts
helm upgrade --install phpldapadmin cetic/phpldapadmin -f phpldapadmin-values.yml
```

cn=ezequiel-farji,dc=gyfcloud,dc=com,dc=ar
cn=ezequiel-farji,ou=people,dc=gyfcloud,dc=com,dc=ar
cn=admin,dc=gyfcloud,dc=com,dc=ar
3xXqvNcI2TTdQroGOq2CcMVORMF1Nx85


#### LDAP user manager
se siguen los pasos de https://github.com/wheelybird/ldap-user-manager
##### yaml de deployment
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: ldap-user-manager
  name: ldap-user-manager
  namespace: default
spec:
  progressDeadlineSeconds: 600
  replicas: 1
  revisionHistoryLimit: 10
  selector:
    matchLabels:
      app: ldap-user-manager
  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
  template:
    metadata:
      labels:
        app: ldap-user-manager
    spec:
      containers:
        - name: ldap-user-manager
          env:
            - name: NO_HTTPS 
              value: "TRUE"
            - name: LDAP_DEBUG 
              value: "TRUE"
            - name: SERVER_HOSTNAME
              value: ldap.cluster.gyfcloud.com.ar
            - name: LDAP_URI
              value: ldap://openldap.default.svc.cluster.local
            - name: LDAP_BASE_DN
              value: dc=gyfcloud,dc=com,dc=ar
            - name: LDAP_REQUIRE_STARTTLS
              value: "FALSE"
            - name: LDAP_ADMINS_GROUP
              value: admins
            - name: LDAP_ADMIN_BIND_DN
              value: cn=admin,dc=gyfcloud,dc=com,dc=ar
            - name: LDAP_ADMIN_BIND_PWD
              value: 3xXqvNcI2TTdQroGOq2CcMVORMF1Nx85 # contraseña obtenida en la configuracion de openldap
            - name: LDAP_USES_NIS_SCHEMA
              value: "true"
            - name: EMAIL_DOMAIN
              value: gyf.com
            - name: SESSION_DEBUG 
              value: "TRUE"
            - name: ACCEPT_WEAK_PASSWORDS 
              value: "TRUE" # Setear en true para habilitar el guardado de contraseñas debiles
          image: wheelybird/ldap-user-manager:v1.2
          imagePullPolicy: IfNotPresent
          resources:
            limits:
              cpu: 200m
              memory: 512Mi
            requests:
              cpu: 100m
              memory: 64Mi
          ports:
            - containerPort: 80
              name: http
              protocol: TCP
            - containerPort: 443
              name: https
              protocol: TCP
          terminationMessagePath: /dev/termination-log
          terminationMessagePolicy: File
      dnsPolicy: ClusterFirst
      restartPolicy: Always
      schedulerName: default-scheduler
      terminationGracePeriodSeconds: 30

---
apiVersion: v1
kind: Service
metadata:
  labels:
    app: ldap-user-manager
  name: ldap-user-manager
  namespace: default
spec:
  ports:
  - name: http
    port: 80
    protocol: TCP
    targetPort: 80
  - name: https
    port: 443
    protocol: TCP
    targetPort: 443
  selector:
    app: ldap-user-manager
  sessionAffinity: None
  type: ClusterIP
---
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  labels:
    app: ldap-user-manager
  name: ldap-user-manager
  namespace: default
spec:
  rules:
  - host: ldap.cluster.gyfcloud.com.ar
    http:
      paths:
      - backend:
          serviceName: ldap-user-manager
          servicePort: 80
        path: /
```
##### Instalacion
```bash
kubectl apply -f ldap-user-manager.yml
```
##### Configuracion
1. ir a la URL https://ldap.cluster.gyfcloud.com.ar:8443/setup y logearse con user admin (y la pass obtenida de OpenLDAP)
2. dejar chequeado por default las opciones para que ldap-user-manager configure los grupos y usuarios de ldap
3. en la siguiente solapa, crear la nueva cuenta de administrador. datos actuales:
        first name: Administrator
        last name: Gyf
        username: administrator-gyf
        password: male-files-Buy-19
        email: infraestructura@gyf.com.ar
4. al crear el usuario redirige a la pagina de login; ingresar con las credenciales recientemente generadas
5. navegar a Account Manager y crear los usuarios y grupos





### Prometheus
#### Archivo values

```yaml
nodeExporter:
  resources:
    limits:
      cpu: 200m
      memory: 50Mi
    requests:
      cpu: 100m
      memory: 30Mi
server:
  retention: "5"
  ingress:
    enabled: true
    hosts: 
     - prometheus.cluster.gyfcloud.com.ar
    # tls:
    #   - secretName: gyf
    #     hosts:
    #       - prometheus.cluster.gyfcloud.com.ar
  ## Prometheus server resource requests and limits
  ## Ref: http://kubernetes.io/docs/user-guide/compute-resources/
  ##
  resources:
    limits:
      cpu: 500m
      memory: 512Mi
    requests:
      cpu: 500m
      memory: 512Mi
```

#### Instalacion por chart

```bash
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
kubectl create ns prometheus
helm upgrade --install prometheus --namespace prometheus prometheus-community/prometheus -f prometheus-values.yml
```

### Nexus
#### Temp ssl secret

```pem
-----BEGIN CERTIFICATE-----
MIID/TCCAeWgAwIBAgICA+gwDQYJKoZIhvcNAQELBQAwRTELMAkGA1UEBhMCQVIx
FTATBgNVBAgMDEJ1ZW5vcyBBaXJlczERMA8GA1UEBwwIQy5BLkIuQS4xDDAKBgNV
BAoMA0d5ZjAeFw0yMDA5MjUxNzUxNDBaFw0zMDA5MjMxNzUxNDBaMBwxGjAYBgNV
BAMMESouZ3lmY2xvdWQuY29tLmFyMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIB
CgKCAQEA3X9oaxZzKJ7PQSw6Y3AjQnaNwUiR3Fq3RKl3K7Wb61sTnLzXcxiirIgD
mITPSY4PBqubHZax7GrbF3U3ocYibqr9Nt9z1dX6wgNMWYXNSJPLEbvGz2/mgPzE
fRVSa8wMTQtSx2z42YQPmWjfhebOyellmaG1UNsQ2iAaQSKTCQZOCUUuqkor8fUz
dNO7Ew6SdEzllBNOhew7p4Fk5qBvlllxtfAeIcPC2yqjp9dmQBaMQafndwt9zMgf
6f4Vmi6JO56+Ga0Edp0u+L1nJdhNlmx2KphgESyJt54KPVVq/C1FNeSN8ueiRb65
eQYyYXAPwprKtilxQ185bYxEc72JkQIDAQABoyAwHjAcBgNVHREEFTATghEqLmd5
ZmNsb3VkLmNvbS5hcjANBgkqhkiG9w0BAQsFAAOCAgEAV+nhTsag4rUZ85BcntUi
F6CHtlYCSshy2yFzCDnMbCrhsItwg4rHJYcnlRvCzUuX4uMIvv6dypakfG0W1gmU
QkqyvRa1j+71SZe0yWBnVxy3VLVZLTME44janLRrZUxibzg2yoAq+u9pFVzz5dmU
tam/aGmHIeWVCO3TJVIUc5U7jl6sXn2H3BHF6ma69dBYC4/X/U/3bCEYEnD0IRUq
GiEpSw0g/DjDOYZi5k4s/3bEPYqvlT31ONhsk36vTHXrrbtl59i1UFGuilUp5k1d
/BGTgXok5pGvd4VcyIYHKLAJ8kcURF7lJCm9FWM0xcVUEO/UXoiWIlrwhzhrsT/B
LTW3kVukPQN52d4zvBT3u9OPObHG1BSt5RuPtyHfad5zjsS/k2PnqtYkjZewL1fl
5lK1cbFryIW+qgT+JY7l4KNF03E457AGNA8C/rmXF9rS03yo17AnSP+Pk2aqd79O
hWL/HfvIi5Vn4CqI9cNPvKruT8x9ZhZq3ScJzyXpEbUmnHMjGlJ4xJTFN+rl8zLa
KMWJqp82wCEzbL63XnhD3KVd2/J/jvcTUf2BH0rY/AEz7ESsrEmN+TP5iiLBMz7g
goog4btORFdlNe5WQekiQ8n45ETb2TcPCzeqFFo9CZnraMbr6NEbZ8SebWO0n7rZ
W0iykukLelsYFUaciehnFQ0=
-----END CERTIFICATE-----
```

```pem
-----BEGIN RSA PRIVATE KEY-----
MIIEpQIBAAKCAQEA3X9oaxZzKJ7PQSw6Y3AjQnaNwUiR3Fq3RKl3K7Wb61sTnLzX
cxiirIgDmITPSY4PBqubHZax7GrbF3U3ocYibqr9Nt9z1dX6wgNMWYXNSJPLEbvG
z2/mgPzEfRVSa8wMTQtSx2z42YQPmWjfhebOyellmaG1UNsQ2iAaQSKTCQZOCUUu
qkor8fUzdNO7Ew6SdEzllBNOhew7p4Fk5qBvlllxtfAeIcPC2yqjp9dmQBaMQafn
dwt9zMgf6f4Vmi6JO56+Ga0Edp0u+L1nJdhNlmx2KphgESyJt54KPVVq/C1FNeSN
8ueiRb65eQYyYXAPwprKtilxQ185bYxEc72JkQIDAQABAoIBAQDVSc6+8AtMXkCt
PW2sO1t4sLioGr16N1CrKZS73lGaPUWRdLby+hILmZ38zEbfSZBml/DyeR4zzmHT
IOjFOwoFD1pt8JaWn0qscIig3BtlfAvbs7oqHlMUelKrMFEBxKdDuVeo+eJXcrei
vj4frrYJ8FoJqpHynkCICsm9VxHHaSQ42mpAsxcGbzuNurpmiPGQX1I1YQgUBF/e
SR5zvQqGDmIm0xOgBmjdqNNVm0Bi/rRz2Q2y8MLqtc7/qEyQLmfm8175NEoVs+Ks
MBJnPDaRlEUVrD1z6WxpKikVv4Vv7IwY+80UQ132zaFpTr12KYxyfO75QBQOhsgp
zDZPwkPBAoGBAO6Pb0bv8PnemeUs39Q8+N6NxW8qlU4MDRZ0vl0j+Ewnp9ALC5r6
md09YheoLWQtZtOr7fCufr+HDYmJrXys4cQZlObcp9gByVaJDK8KHhgkoonQ4o8g
aPgdTLx/ZwNDoRy9lLxXr5/L7jkpUGi7VMN2MfNoCGZjJZg1KJ7cy2uNAoGBAO2w
py1wZoCOKcqu3bAE6oZIkGBfxpukes2hlRt4v6xigR1OUUQqrS3a2g0E6S9FGtDd
dMT4GUES0xDUPMB7/zFGVtvHg26mpmkkDJCd3eH8cilSB6XwyOO+zL48+sXVtszi
I6Boiu64RUOEKywz7V4Q4uuxTqH2lucrOnK8q1MVAoGAT8iMsK7Cau61jV3KpdKW
cOf9JcaaFHYXUnFM0R2chuukRgHLqdMx7jn/OmY2eBBvGU+gziOtpCRotJzk9pNj
FhBwKh2VzLocOeKv6UJ6+hwWd/zyKGfwczppVYlOTFsr4M7OlYbF561uSJO2nrA4
Ev7Oidc7J/L0ucjdgvPY3GkCgYEAv86sOdjYfqk6RFs0flSZRN/N0le3l+X1S3c3
MkIQuvWKp7GJlu6xba2MBLGP0xt69k2fa0uiZYbhWFVM6S+m0YanlRe9kk9dPr9e
fF1IrpLfNUUC8wAJvO89nc7rtzYtG8zpXwq+AsIk8sT3aeZFiIEr/XLJt52cTNPv
iKODxbkCgYEAlvPqZ6yD83s5oN+D4cApSCNdc0ByI8HatBzZZac4eCrfMUCdBb3u
99bdPuMC22j429uqlq/pLPybKlMHs9hpbDaE0acVXd4tGl/bjlkXnUMOlzW5xp2j
kK8zcOwaRBrhfnpTa3QIDSgqHTMh1ZcUIEU1IYNexCvU9geikAtuu4I=
-----END RSA PRIVATE KEY-----
```

```
kubectl create secret tls gyfcloud --cert=/home/ansible/git/ansible/kubespray/inventory-cloud/gyfcloud.crt --key=/home/ansible/git/ansible/kubespray/inventory-cloud/gyfcloud.key
```
#### Archivo values
```yaml
nexus:
  context: nexus
  readinessProbe:
    path: /nexus
  livenessProbe:
    path: /nexus
  service:
    type: ClusterIP 
  resources:
    requests:
      memory: 4Gi 
      cpu: 250m 
    limits:
      memory: 4Gi 
      cpu: 1 
ingress:
  annotations: 
    nginx.ingress.kubernetes.io/proxy-body-size: 50m # IMPORTANTE: sin esto, nginx va a rechazar el pusheo de paquetes que pesen más que el tamaño estandar
  enabled: true 
  path: /nexus
  tls:
    enabled: true
    secretName: gyfcloud
nexusProxy:
  enabled: no
  env:
    nexusHttpHost: cluster.gyfcloud.com.ar 
      
```
#### Instalacion por chart

##### Chart

```bash
helm repo add oteemocharts https://oteemo.github.io/charts
helm upgrade --install sonatype-nexus oteemocharts/sonatype-nexus -f nexus-values.yml

# --set ingress.enabled=true --set nexusProxy.env.nexusHttpHost=nexus.gyf.com --set ingress.tls.secretName=nexus
```

#### Post-instalacion
Configurar nexus: https://github.com/travelaudience/kubernetes-nexus/blob/master/docs/admin/configuring-nexus.md

#### Migracion por api
1. Revisar que este habilitado el realm de NugetApiKey en Security/Realms
2. Obtener el apikey de Nuget para un usuario con permisos, por ejemplo admin. Esto se obtiene entrando al perfil del usuario desde el navegador, y eligiendo en el panel la sección de Nuget ApiKey

#### Migracion por storage
Stoppear ambos Nexus, el nuevo donde tomaremos los archivos y el viejo donde realizaremos el restore.

Para hacer mas rapido la transferencia de arcdhivos se puede montar el storage de l007

```=bash
export OLDSTORAGE='/mnt/oldstorage'
cat "//storage/cluster-storage $OLDSTORAGE cifs rw,username=k8s-allaccess,password=gyf754,uid=0,gid=0,file_mode=0777,dir_mode=0777 0 0" >> /etc/fstab
sudo mount -a

rsync -avP --del /mnt/oldstorage/.../nexus-data/blobs/gyf-blobs/ /mnt/newstorage/pvc-kubernetes/nexus-data/blobs/

# Copiar los archivos de backup correspondiente al ultimo dia generados en Nexus
rsync -avP --del /mnt/oldstorage/.../nexus-data/backup /mnt/newstorage/pvc-kubernetes/nexus-data/restore-from-backup
```

Para que haga el restore from backup debe eliminarse la base que se genero automaticamente en el primer inicio, en caso de haberse hecho. En tal caso debe eliminarse las carpetas:

```=bash
rm -rf ~nexus-data/db/security
rm -rf ~nexus-data/db/config
rm -rf ~nexus-data/db/component
```

Startear el nuevo nexus

#### Troubleshooting

Si montas mal el storage y aparece como `"device not found"` o `d?????????` tenes que hacer umount de ese storage, pero tal vez eso tambien te devuelva un mensaje como `device busy` y ahi tenes que tirar el umount con el flag -l `umount -a -l`

### Harbor
#### Archivo values
```yaml
expose:
  type: ingress
  tls:
    enabled: false
    secretName: harbor
  ingress:
    hosts:
      core: core.harbor.cluster.gyfcloud.com.ar
      notary: notary.harbor.cluster.gyfcloud.com.ar
externalURL: https://core.harbor.cluster.gyfcloud.com.ar
imagePullPolicy: IfNotPresent
persistence:
  resourcePolicy: "delete"

database:
  type: internal
# debug, info, warning, error or fatal
logLevel: info

secretKey: "not-a-secure-key"
core:
  replicas: 2
  resources:
    requests:
      memory: 128Mi
      cpu: 100m
    limits: 
      memory: 768Mi
registry:
  replicas: 1
  resources:
    requests:
      memory: 128Mi
      cpu: 100m
    limits: 
      memory: 768Mi
```
#### Instalacion por chart
se instala el chart Harbor para usar como docker registry

> **IMPORTANTE: el chart oficial de harbor no modifica los permisos de la base de datos (postgre) al inicializar; solo modifica el owner, pero postgre requiere la informacion tenga un modo especial (0700). por lo tanto, lo conveniente sera usar una base de postgre existente**
```bash
kubectl create ns harbor
# kubectl create secret tls harbor --cert=cert/tls.cert --key=cert/tls.key --namespace harbor
# kubectl create secret generic ca --from-file=ca.crt=cert/Gyf_RCA.crt --namespace harbor
helm repo add harbor https://helm.goharbor.io
helm repo update
helm upgrade --install harbor --namespace harbor harbor/harbor -f harbor-values.yml
```

### Gitlab
```bash
kubectl create secret tls -n gitlab gyfcloud --cert=/home/ansible/git/ansible/kubespray/inventory-cloud/gitlab.gyfcloud.crt --key=/home/ansible/git/ansible/kubespray/inventory-cloud/gitlab.gyfcloud.key
```


#### Archivo values
```yaml
global:
  grafana:
    enabled: false
  edition: ce
  time_zone: America/Argentina/Buenos_Aires
  hosts:
    domain: gitlab.cluster.gyfcloud.com.ar
  ingress:
    class: nginx
    configureCertmanager: True
    annotations:
      kubernetes.io/tls-acme: True
      # nginx.ingress.kubernetes.io/proxy-body-size: 100m
    tls:
      enabled: true
postgresql:
  existingSecret: false
nginx-ingress:
  enabled: false
certmanager:
  install: False
gitlab-runner:
  install: false
  runners:
    privileged: true
gitlab:
  webservice:
    ingress:
      tls:
        enabled: false
registry:
  ingress:
    tls:
      enabled: false
minio:
  ingress:
    tls:
      enabled: false
```
#### Instalacion por chart
se instala el chart Gitlab 
```bash
kubectl create ns gitlab
helm repo add gitlab https://charts.gitlab.io/
helm repo update
helm upgrade --install gitlab --namespace gitlab gitlab/gitlab -f gitlab-values.yaml --version 4.4.1
```

#### Gitlab webserver
credenciales iniciales:

Usuario: **root**  
Password: 
```bash
kubectl get secret gitlab-gitlab-initial-root-password -ojsonpath="{.data.password}" -n gitlab | base64 --decode 
```

#### Minio

credenciales iniciales:  
```bash
# AccessKey: 
kubectl get secret -n gitlab gitlab-minio-secret -o jsonpath="{.data.accesskey}" | base64 --decode

# SecretKey: 
kubectl get secret -n gitlab gitlab-minio-secret -o jsonpath="{.data.secretkey}" | base64 --decode
```
